from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from albums.models.album import Album


class Like(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='likes')
    album = models.ForeignKey(Album, on_delete=models.CASCADE, related_name='likes')
    timestamp = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ['-timestamp']
        constraints = [models.UniqueConstraint(fields=['user', 'album'], name='uniq_like')]

    def __str__(self):
        return f'{self.user.username} likes {self.album.name}'
