import mutagen as mutagen
from django.db import models

from albums.models.album import Album


class Track(models.Model):
    name = models.CharField(max_length=120)
    album = models.ForeignKey(Album, on_delete=models.CASCADE, related_name='tracks')
    file = models.FileField()
    duration = models.IntegerField()

    def save(self, *args, **kwargs):
        audio_info = mutagen.File(self.file).info
        self.duration = int(audio_info.length)

        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.album.artist.name} - {self.name} ({self.album.name})'
