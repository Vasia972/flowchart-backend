from django.db import models

from albums.models.album import Album


class Bond(models.Model):
    """Связь между альбомами."""
    tag = models.CharField(max_length=25)
    parent = models.ForeignKey(Album, on_delete=models.CASCADE, related_name='bonds')
    child = models.ForeignKey(Album, on_delete=models.CASCADE, related_name='parent_bonds')
    backward_bond = models.OneToOneField('self', on_delete=models.CASCADE, null=True)

    def save(self, **kwargs):
        i_tag = self.tag.lower()
        is_comparable = i_tag.startswith('more ') or i_tag.startswith('less ')
        if is_comparable and self.backward_bond is None:
            if i_tag.startswith('more '):
                backward_tag = i_tag.replace('more ', 'less ', 1)
            else:
                backward_tag = i_tag.replace('less ', 'more', 1)

            self.backward_bond = Bond(
                tag=backward_tag,
                parent=self.child,
                child=self.parent,
                backward_bond=self,
            )
            self.backward_bond.save()

        super().save(**kwargs)
