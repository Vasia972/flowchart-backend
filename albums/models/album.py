from django.db import models

from albums.models.artist import Artist
from albums.models.genre import Genre


class Album(models.Model):
    name = models.CharField(max_length=120)
    description = models.TextField()
    cover = models.ImageField(null=True)
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE, related_name='albums')
    genre = models.ForeignKey(Genre, on_delete=models.SET_NULL, related_name='albums', null=True)

    def __str__(self):
        return f'{self.artist.name} - {self.name}'
