from dataclasses import dataclass
from typing import List, Optional


@dataclass
class TrackData:
    name: str
    file: str
    duration: int


@dataclass
class GenreData:
    name: str


@dataclass
class BondData:
    id: int
    tag: str
    album_id: int


@dataclass
class AlbumData:
    id: int
    name: str
    description: str
    cover: str
    artist: str
    genre: str
    tracks: List[TrackData]
    children: List[BondData]
    liked: Optional[bool] = None
