from django.http import Http404
from rest_framework.exceptions import NotFound
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from albums.models.album import Album
from albums.serializers.album import AlbumSerializer
from albums.service import AlbumService


class RandomAlbumView(APIView):
    def get(self, request, format=None):
        user = request.user.is_authenticated and request.user or None

        album = AlbumService.get_random_album(user=user)

        if not album:
            return Response({'detail': 'Альбомов пока еще не добавлено.'}, status=404)

        return Response(AlbumSerializer(album).data)


class AlbumDetailView(APIView):
    def get(self, request, pk, format=None):
        user = request.user.is_authenticated and request.user or None

        album = AlbumService.get_album(pk, user=user)

        if not album:
            raise Http404

        serializer = AlbumSerializer(album)
        return Response(serializer.data)


class AlbumListView(APIView):
    def get(self, request, format=None):
        albums = AlbumService.get_all_albums()
        serializer = AlbumSerializer(albums, many=True)
        return Response(serializer.data)


class AlbumLikeView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, pk, format=None):
        try:
            AlbumService.add_like(request.user, pk)
        except Album.DoesNotExist:
            raise NotFound
        return Response()


class AlbumDislikeView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, pk, format=None):
        AlbumService.remove_like(request.user, pk)
        return Response()


class LikedAlbumsView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        albums = AlbumService.get_liked_albums(request.user)
        serializer = AlbumSerializer(albums, many=True)
        return Response(serializer.data)
