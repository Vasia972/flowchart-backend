import random
from typing import Optional, List

from django.contrib.auth.models import User
from django.db import IntegrityError
from django.db.models import Max

from albums.dto import AlbumData, TrackData, BondData, GenreData
from albums.models.album import Album
from albums.models.bond import Bond
from albums.models.genre import Genre
from albums.models.likes import Like


class AlbumService:
    album_query = Album.objects.prefetch_related('artist', 'genre', 'tracks')

    @classmethod
    def get_all_genres(cls) -> List[GenreData]:
        return [GenreData(name=item.name) for item in Genre.objects.all()]

    @classmethod
    def get_all_albums(cls) -> List[AlbumData]:
        albums = cls.album_query.all()
        return [cls._create_album_data(album) for album in albums]

    @classmethod
    def get_random_album(cls, user: Optional[User] = None) -> Optional[AlbumData]:
        if not Album.objects.exists():
            return None

        max_id = Album.objects.all().aggregate(max_id=Max('id'))['max_id']
        while True:
            pk = random.randint(1, max_id)
            album = cls.album_query.filter(pk=pk).first()
            if album:
                return cls._create_album_data(album, user)

    @classmethod
    def get_album(cls, pk: int, user: Optional[User] = None) -> Optional[AlbumData]:
        try:
            album = cls.album_query.get(pk=pk)
            return cls._create_album_data(album, user=user)
        except Album.DoesNotExist:
            return None

    @classmethod
    def add_like(cls, user: User, album_id: int) -> None:
        album = Album.objects.get(pk=album_id)
        try:
            Like(album=album, user=user).save()
        except IntegrityError as e:
            if 'UNIQUE constraint failed' not in str(e):
                raise

    @classmethod
    def remove_like(cls, user: User, album_id: int) -> None:
        Like.objects.filter(user=user, album=album_id).delete()

    @classmethod
    def get_liked_albums(cls, user: User) -> List[AlbumData]:
        likes = Like.objects.filter(user=user).prefetch_related('album')
        return [cls._create_album_data(like.album) for like in likes]

    @staticmethod
    def _create_album_data(album: Album, user: Optional[User] = None) -> AlbumData:
        bonds = Bond.objects.filter(parent=album).select_related('child')
        liked = user and Like.objects.filter(album=album, user=user).exists() or None

        return AlbumData(
            id=album.id,
            name=album.name,
            description=album.description,
            cover=album.cover.url,
            artist=album.artist.name,
            genre=album.genre.name,
            tracks=[
                TrackData(name=track.name, file=track.file, duration=track.duration)
                for track in album.tracks.all()
            ],
            children=[
                BondData(id=bond.id, tag=bond.tag, album_id=bond.child.id)
                for bond in bonds
            ],
            liked=liked,
        )
