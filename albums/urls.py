from django.urls import path

from albums import views


urlpatterns = [
    path('', views.AlbumListView.as_view()),
    path('<int:pk>', views.AlbumDetailView.as_view()),
    path('random', views.RandomAlbumView.as_view()),

    path('liked', views.LikedAlbumsView.as_view()),
    path('<int:pk>/like', views.AlbumLikeView.as_view()),
    path('<int:pk>/dislike', views.AlbumDislikeView.as_view()),
]
