from rest_framework import serializers

from albums.serializers.bond import AlbumBondSerializer
from albums.serializers.track import AlbumTrackSerializer


class AlbumListItem(serializers.Serializer):
    name = serializers.CharField(max_length=120)
    cover = serializers.CharField(max_length=500)
    artist = serializers.CharField(max_length=120)
    genre = serializers.CharField(max_length=120)

    def update(self, instance, validated_data):
        pass


    def create(self, validated_data):
        pass


class AlbumSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=120)
    description = serializers.CharField()
    cover = serializers.CharField(max_length=500)
    artist = serializers.CharField(max_length=120)
    genre = serializers.CharField(max_length=120)
    tracks = AlbumTrackSerializer(many=True)
    children = AlbumBondSerializer(many=True)
    liked = serializers.BooleanField()

    def update(self, instance, validated_data):
        pass


    def create(self, validated_data):
        pass
