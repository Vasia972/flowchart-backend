from rest_framework import serializers


class AlbumTrackSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=120)
    file = serializers.FileField()
    duration = serializers.IntegerField()

    def update(self, instance, validated_data):
        pass


    def create(self, validated_data):
        pass
