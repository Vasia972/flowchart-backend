from rest_framework import serializers


class AlbumBondSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    tag = serializers.CharField(max_length=25)
    album_id = serializers.IntegerField()

    def update(self, instance, validated_data):
        pass


    def create(self, validated_data):
        pass
