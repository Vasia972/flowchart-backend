from django.contrib import admin

from albums.models.album import Album
from albums.models.artist import Artist
from albums.models.bond import Bond
from albums.models.genre import Genre
from albums.models.track import Track

from django import forms


class BondModelForm(forms.ModelForm):

    backward_tag = forms.CharField(label='Обратная связь')

    def save(self, commit=True):
        instance = super().save(commit=False)

        backward_tag = self.cleaned_data.get('backward_tag')

        if backward_tag is not None:
            instance.save()

            backward_bond = Bond.objects.create(
                tag=backward_tag,
                parent=self.cleaned_data['child'],
                child=self.cleaned_data['parent'],
                backward_bond=instance,
            )
            instance.backward_bond = backward_bond
            instance.save()

        return instance

    class Meta:
        model = Bond
        fields = ['tag', 'parent', 'child']


@admin.register(Bond)
class BondModelAdmin(admin.ModelAdmin):

    form = BondModelForm

    fieldsets = (
        (None, {
            'fields': ('tag', 'parent', 'child', 'backward_tag',),
        }),
    )


admin.site.register(Album)
admin.site.register(Artist)
admin.site.register(Genre)
admin.site.register(Track)
# admin.site.register(Bond)
