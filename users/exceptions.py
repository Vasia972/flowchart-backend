class UserAlreadyExists(Exception):
    pass


class AuthenticationFailed(Exception):
    pass
