from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView

from users import views


urlpatterns = [
    path('info', views.UserInfoView.as_view()),
    path('register', views.UserRegistrationView.as_view()),
    path('token/obtain', TokenObtainPairView.as_view()),
    path('token/refresh', TokenRefreshView.as_view()),
]

