from typing import Tuple

from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

from users.dto import UserRegisterData, UserLoginData
from users.exceptions import UserAlreadyExists, AuthenticationFailed


class UserService:
    @staticmethod
    def create_user(user_register_data: UserRegisterData):
        if User.objects.filter(username=user_register_data.username).exists():
            raise UserAlreadyExists('Пользователь с таким именем уже существует')

        User.objects.create_user(
            username=user_register_data.username,
            password=user_register_data.password,
            email=user_register_data.email,
        )

    @staticmethod
    def auth_user(user_login_data: UserLoginData) -> Tuple[User, Token]:
        user = authenticate(
            username=user_login_data.username,
            password=user_login_data.password,
        )

        if not user:
            raise AuthenticationFailed('Не удалось найти пользователя с таким логином и паролем.')

        token, created = Token.objects.get_or_create(user=user)

        return user, token
