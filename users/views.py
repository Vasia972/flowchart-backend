from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from users.dto import UserRegisterData
from users.exceptions import UserAlreadyExists
from users.serializers import UserSerializer, UserDataSerializer
from users.service import UserService


class UserRegistrationView(APIView):
    def post(self, request):
        serializer = UserSerializer(data=request.data)

        serializer.is_valid(raise_exception=True)

        try:
            UserService.create_user(UserRegisterData(
                username=serializer.data['username'],
                password=serializer.data['password'],
                email=serializer.data['email'],
            ))
            return Response(status=status.HTTP_201_CREATED)
        except UserAlreadyExists as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)


class UserInfoView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        serializer = UserDataSerializer(request.user)
        return Response(serializer.data)
