from dataclasses import dataclass
from typing import Optional


@dataclass
class UserRegisterData:
    username: str
    password: str
    email: Optional[str]


@dataclass
class UserLoginData:
    username: str
    password: str
