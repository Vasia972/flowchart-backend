FROM python:3.7-alpine

WORKDIR /opt/backend

# устанавливаем системные зависомости
RUN python -m pip install --upgrade pip

RUN apk update && apk add gcc musl-dev python3-dev jpeg-dev zlib-dev libffi-dev openssl-dev cargo

# устанавливает WSGI сервер
RUN pip install gunicorn

# устанавливаем зависимости
COPY requirements.txt /opt/backend/
RUN pip install -r requirements.txt --no-cache-dir

# копируем исходный код
COPY . .

# запускаем gunicorn при старте контейнера
CMD exec gunicorn --pythonpath=/opt/backend --bind 0.0.0.0:5000 --workers=2 --name musicflowchart musicflowchart.wsgi:application
